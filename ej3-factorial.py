"""
Cree un programa que solicite al usuario un número entre 0 y 30.
El programa debe calcular el factorial (N!)
del número ingresado por el usuario.
"""

# Entradas de usuario
num = int(input("Digite un número entre 0 y 30 : "))

factorial = num

while num > 1:
    factorial *= num-1
    num -= 1

print(factorial)
