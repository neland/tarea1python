"""
Cree un programa que imprima la tabla
de multiplicación desde el 0 hasta el 20
mostrando el siguiente formato: 0 X 0 = 0
"""

def multiplicar_numeros():
    for i in range(0, 21):
        print("\n***************")
        print("TABLA DEL: {}".format(i))
        print("***************")
        for j in range(0, 21):
            print("{} x ".format(i) + "{} = {}".format(j, (i*j)))

multiplicar_numeros()
