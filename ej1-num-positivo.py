"""
Cree un programa que solicite dos números.
Verifique que estos números son positivos y
verifique que el primero número es menor por
al menos 50 que el segundo número.
Si los números ingresados son correctos,
el programa debe calcular la suma de todos los
números entre el primer número y el segundo
que son múltiplos de 5 y múltiplos de 3.
"""
diferencia_entre_nums = 50
mult_3 = 3
mult_5 = 5

# Entradas de usuario
primer_numero = float(input("Digite el primer número : "))
segundo_numero = float(input("Digite el segundo número : "))


def verificar_positivos():
    if primer_numero and segundo_numero > 0:
        return True
    else:
        return False


def verificar_diferencia():
    if segundo_numero - primer_numero >= diferencia_entre_nums:
        return True
    else:
        return False


def calcular_multiplos():
    suma = 0
    contador = primer_numero
    while(contador <= segundo_numero):
        if contador % mult_3 == 0:
            suma = suma + primer_numero
        elif contador % mult_5 == 0:
            suma = suma + contador
        contador += 1
    return suma


if verificar_positivos() and verificar_diferencia():
    suma_total = calcular_multiplos()
    print("*" * 50 + "\n La suma total es : {}"
          .format(suma_total) + "\n" + "*" * 50)
else:
    print("Números incorrectos, digite otro par de números")

